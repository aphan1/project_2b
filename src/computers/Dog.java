/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */
package computers;

import core.ComputerPlayer;
import core.Player;
import game.Rank;
import game.StateHandler;
import game.World;

/** An AI player named Dog.
 */
public class Dog extends ComputerPlayer {
	/** Name of the AI player. */
	private static final String KART_NAME = "Dog";
	/** Full file location of the kart's image. */
	private static final String IMAGE_FILE = StateHandler.ASSETS_PATH + "/karts/dog.png";
	/** Percentage of the default kart accceleration, when Dog is ahead of the
	 * main player. */
	private static final double ACCELERATION_BEHIND = 0.9;
	/** Percentage of default kart accceleration, when Dog is behind the
	 * main player. */
	private static final double ACCELERATION_AHEAD = 1.1;

	/** Create a new Dog AI player. */
	public Dog() {
		super(KART_NAME, IMAGE_FILE);
	}

	/** Move/accelerate to a new position in the game.
	 * @param world The world the AI player is in.
	 */
	public void move(World world)
	{
		Rank rank = world.getRank();

		// If the AI kart is ahead of the main player
		if (rank.getRank(this) < rank.getRank(world.getMainPlayer())) {
			setAcceleration(ACCELERATION_BEHIND * Player.DEFAULT_ACCELERATION);
		// If AI kart is behind
		} else {
			setAcceleration(ACCELERATION_AHEAD * Player.DEFAULT_ACCELERATION);
		}

		super.move(world);
	}
}
