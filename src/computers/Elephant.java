/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */
package computers;

import core.ComputerPlayer;

/** An AI player named Elephant.
 */
public class Elephant extends ComputerPlayer {
	/** Name of the AI player. */
	private static final String KART_NAME = "Elephant";
	/** Full file location of the kart's image. */
	private static final String IMAGE_FILE = "assets/karts/elephant.png";

	/** Create a new Elephant AI player. */
	public Elephant() {
		super(KART_NAME, IMAGE_FILE);
	}
}
