/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */
package computers;

import java.awt.geom.Point2D;

import core.ComputerPlayer;
import game.World;

/** An AI player named Octopus. */
public class Octopus extends ComputerPlayer {
	/** Name of the AI player. */
	public static final String KART_NAME = "Octopus";
	/** Full file location of the kart's image. */
	private static final String IMAGE_FILE = "assets/karts/octopus.png";
	/** Minimum radius range to the player, such that the AI kart will
	 * crash towards the main player. */
	private static final double CRASH_RANGE_MIN = 100;
	/** Maximum radius range to the player, such that it will crash
	 * to the main player. */
	private static final double CRASH_RANGE_MAX = 250;

	/** Creates a new Octopus AI player. */
	public Octopus() {
		super(KART_NAME, IMAGE_FILE);
	}

	/** Move/accelerate to a new position in the game.
	 * @param world The world the AI player is in.
	 */
	public void move(World world)
	{
		// Get the distance from Octopus to the main player
		Point2D.Double main_player = world.getMainPlayer().getPosition();
		double dist = this.getPosition().distance(main_player);

		// If Octopus is within crash range, crash into player. Otherwise, move
		// to the current waypoint
		if (dist >= CRASH_RANGE_MIN && dist <= CRASH_RANGE_MAX) {
			super.move(world, world.getMainPlayer().getPosition());
		} else {
			super.move(world);
		}
	}
}
