/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */ 
package game;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;

import states.Credits;
import states.Game;
import states.MainMenu;
import states.SelectKarts;
import twl.TWLStateBasedGame;

/** Represents the handler of all game states.
 * Only one instance of this class needs to exist.
 */
public class StateHandler extends TWLStateBasedGame {
	/** Singleton instance of StateHandler. */
	private static StateHandler _instance = null;
	/** Singleton instance of AppGameContainer. */
	private static AppGameContainer _container = null;

    /** The name of the game. */
    public static final String GAME_NAME = "Shadow Kart";
    /** The working directory of this game. */
    public static final String WORKING_DIR = System.getProperty("user.dir");
    /** Location of the "assets" directory. */
    public static final String ASSETS_PATH = "assets";
    /** Location of the "data" directory */
    public static final String DATA_PATH = "data";
    /** Full file path for the theme file of TML. */
    private static final String THEME_FILE =
    		WORKING_DIR + "\\" + ASSETS_PATH + "\\twl\\chutzpah.xml";

    /** Default font name for titles. */
    public static final String FONT_NAME = "Segoe UI";

    /** Default screen width, in pixels. */
    private static final int SCREENWIDTH = 800;
    /** Default screen height, in pixels. */
    private static final int SCREENHEIGHT = 600;

    /** Maximum number of players in a game */
    public static final int MAX_PLAYERS = 8;
    /** The default number of players, including the human player */
    public static final int DEFAULT_PLAYERS = 4;

    /** Game state for Shadow Kart game. */
	private static Game game;
	/** Game state for the main menu. */
	private static MainMenu main_menu;
	/** Game state for customising a custom game. */
	private static SelectKarts select_karts;
	/** Game state for credits. */
	private static Credits credits;

    /** Create a new StateHandler object. 
     * @throws SlickException */
    public StateHandler()
    throws SlickException {
        super(GAME_NAME);

        game = new Game();
        main_menu = new MainMenu();
        select_karts = new SelectKarts();
        credits = new Credits();
    }

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		// Add all game states
		this.addState(main_menu);
		this.addState(game);
		this.addState(select_karts);
		this.addState(credits);
	}

	/** Get a single instance of <code>StateHandler</code>. */
	public static StateHandler getInstance() {
		if (_instance == null) {
			try {
				_instance = new StateHandler();
				_container = new AppGameContainer(_instance);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}

		return _instance;
	}

	/** Get the current width of the game screen. */
	public static int getWidth() {
		return _container.getWidth();
	}

	/** Get the current height of the game screen. */
	public static int getHeight() {
		return _container.getHeight();
	}

	/** Get the <code>Game</code> of Shadow Kart. */
	public static Game getGame() {
		return game;
	}

	/** Get the <code>SelectKarts</code> of Shadow Kart. */
	public static SelectKarts getSelectKarts() {
		return select_karts;
	}

	/** Get the <code>MainMenu</code> of Shadow Kart. */
	public static MainMenu getMainMenu() {
		return main_menu;
	}

	/** Get the <code>Credits</code> of Shadow Kart. */
	public static Credits getCredits() {
		return credits;
	}

    /** Start-up method. Creates the game and runs it.
     * @param args Command-line arguments (ignored).
     */
    public static void main(String[] args)
    throws SlickException
    {
    	getInstance();
        // setShowFPS(true), to show frames-per-second.
    	_container.setTargetFrameRate(60);
    	_container.setShowFPS(true);
    	_container.setDisplayMode(SCREENWIDTH, SCREENHEIGHT, false);
    	_container.start();
    }

	@Override
	protected URL getThemeURL() {
		try {
			// Load up the theme file and return it
			File xml_file = new File(THEME_FILE);
			return xml_file.toURI().toURL();
		} catch (MalformedURLException e) {}

		return null;
	}
}

