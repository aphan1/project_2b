/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */ 
package game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.newdawn.slick.Graphics;

import core.Kart;

/** Handles the kart's race positions in Shadow Kart
 */
public class Rank {
	/** Message when the human player finishes the race. */
	private static final String FINISH_MESSAGE = "You came ";
	/** Where the finish message will be placed on the screen. */
	private static final int[] MESSAGE_POSITION =
		{StateHandler.getWidth()/2, StateHandler.getHeight()/5};
	/** A list of all karts in a game. */
	private ArrayList<Kart> all_karts;
	/** The current last player's ranking to have finished the race. */
	private int last_rank_finish;


	/** Create a new Rank object for a <code>World</code>.
	 * @param world The world the <code>Rank</code> instance handles the
	 * rankings of all karts in the game.
	 */
	public Rank(World world) {
		all_karts = new ArrayList<Kart>(world.getComputers());
		all_karts.add(world.getMainPlayer());
	}

	/** Get a list of karts, sorted by their y-coordinates. */
	public ArrayList<Kart> getKartsSorted() {
		// Sort only karts who have not finished the race, based on their
		// y-positions in the game.
		if (last_rank_finish < all_karts.size()) {
			Collections.sort(all_karts.subList(last_rank_finish, all_karts.size()),
		    		new Comparator<Kart>() {
		    			public int compare(Kart kart_1, Kart kart_2) {
		    				return (int)(kart_1.getPosition().y - kart_2.getPosition().y);
		    			}
		    });

			// If a unfinished player
	  		if (all_karts.get(last_rank_finish).isFinished()) {
	  			last_rank_finish++;
	  		}
		}

		// Returned a cloned list, in case someone tampers the sorted list
  		return new ArrayList<Kart>(all_karts);
    }

	/** Get the rank of a kart in the game. */
    public int getRank(Kart kart) {
    	return getKartsSorted().indexOf(kart) + 1;
    }

    /** Draw the finishing message for the main player.
     * @param world The world's <code>Rank</code> instance.
     * @param g Refer to <code>World.render(...)</code>.
     */
    public void render(World world, Graphics g) {
    	if (!world.getMainPlayer().isFinished()) return;

    	// If the player has finished the race, render the finishing message.
    	int player_rank = getRank(world.getMainPlayer());
    	String finish_text = FINISH_MESSAGE + Panel.ordinal(player_rank) + "!";

    	g.drawString(finish_text,
    			MESSAGE_POSITION[0] - g.getFont().getWidth(finish_text)/2,
    			MESSAGE_POSITION[1]);
    }
}
