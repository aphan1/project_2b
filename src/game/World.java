/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Matt Giuca <mgiuca>
 * Modified by: Andy Phan <aphan1>
 */
package game;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

import computers.*;
import core.ComputerPlayer;
import core.Item;
import core.Kart;
import core.Player;
import core.TempObject;
import states.Game;

/** Represents the entire game world.
 * (Designed to be instantiated just once for the whole game).
 */
public class World
{
	/** Location of the TMX map file */
	private static final String TMX_PATH = StateHandler.ASSETS_PATH + "/map.tmx";
	/** Location of the item data file */
	private static final String ITEMS_PATH = StateHandler.DATA_PATH + "/items.txt";
	/** Location of the waypoints data file */
	private static final String WAYPOINTS_PATH = StateHandler.DATA_PATH + "/waypoints.txt";
	/** Location of the kart's starting locations */
	private static final String STARTPOINTS_PATH = StateHandler.DATA_PATH + "/startpoints.txt";

	/** A property of a tile from a tile from TiledMap, particularly its friction */
	private static final String PROPERTY_FRICTION = "friction";
    /** The coefficient of friction of a blocking tile */
	private static final int BLOCKING_TILE_FRICTION = 1;
	/** The maximum distance between the player and any other kart,
	 * such that it is considered a collision. */
	private static final double COLLISION_RANGE = 40;
	/** The y-coordinate for the finish line location */
	public static final double FINISH_LINE_Y  = 1026;

    /** Class which reads the map file */
	private TiledMap map;
	/** Class holding the point to centre on in the game */
	private Camera camera;
	/** Status panel for the main player */
	private Panel panel;

	/** Main human player of the game. */
	private Player main_player;
	/** Computer players in the game. */
	private ArrayList<ComputerPlayer> computers;
	/** Position of players, based on player number */
	private ArrayList<Point2D.Double> start_positions;
	/** Waypoints all computer players share */
	private ArrayList<Point2D.Double> waypoints;

	/** Current unpicked items left in the world. */
	private ArrayList<Item> items;
	/** Items active effects activated by player(s). */
	private ConcurrentLinkedQueue<TempObject> active_effects;
	/** A Rank instance which handles the ranks of players in the race. */
	private Rank rank;

    /** Create a new World object.
     */
    public World() 
    throws SlickException
    {
    	// Load up the default waypoints for an AI player to follow
    	waypoints = ComputerPlayer.getPoints(WAYPOINTS_PATH);
    }

    /** Start/restart a game of Shadow Kart.
     * @param kartData The karts to be in the game.
     */
    public void start(Kart[] kartData)
    throws SlickException {
    	main_player = null;
        map = new TiledMap(TMX_PATH);
        items = Item.getItems(ITEMS_PATH);
        panel = new Panel();
        computers = new ArrayList<ComputerPlayer>();
        active_effects = new ConcurrentLinkedQueue<TempObject>();
        start_positions = ComputerPlayer.getPoints(STARTPOINTS_PATH);

        if (kartData == null)
        	addPlayersDefault();
        else
        	addAllPlayers(kartData);

        camera = new Camera(this, main_player.getPosition());
        rank = new Rank(this);
    }

    /** Add the players, when given the default options. */
    private void addPlayersDefault()
    {
    	addPlayer(new Player());
    	addPlayer(new Elephant());
    	addPlayer(new Dog());
    	addPlayer(new Octopus());
    }

    /** Add all players on the game, based on an array of karts.
     * @param kartData An array of karts.
     */
    private void addAllPlayers(Kart[] kartData) {
    	for (Kart k : kartData) {
    		addPlayer(k);
    	}
    }

    /** 
     * @param kart
     */
    private void addPlayer(Kart kart) {
    	// Get the number of players, and its starting point based on that
    	// number
    	int player_total = (main_player != null ? 1 : 0) + computers.size();
    	Point2D.Double start_point = start_positions.get(player_total);
    	
    	// Reset the kart's properties
    	kart.reset();

    	if (kart instanceof Player)
    		main_player = (Player)kart;
    	else if (kart instanceof ComputerPlayer)
    		computers.add((ComputerPlayer)kart);
    	else
    		return;

    	// Set the kart's position as the starting point
    	kart.setPosition(start_point);
    }

    /** TODO: FIX THIS DOCUMENTATION
    /** Update the game state for a frame.
     * @param rotate_dir The player's direction of rotation
     *      (-1 for anti-clockwise, 1 for clockwise, or 0).
     * @param move_dir The player's movement in the car's axis (-1, 0 or 1).
     * @param use_item Indicates if player wants to use item
     * @param game
     */
    public void update(double rotate_dir, double move_dir, boolean use_item, boolean paused)
    throws SlickException
    {
    	// Update nothing if game is paused
    	if (paused)
    		return;

    	// Leave it up to the Player.update to change its position
    	main_player.move(this, rotate_dir, move_dir, use_item);

    	// Update all computer player's positions
    	for (ComputerPlayer ai : computers) {
    		ai.move(this);
    	}

    	// If a player has crossed the finish line
    	for (Kart kart : rank.getKartsSorted()) {
    		if (kart.getPosition().y < FINISH_LINE_Y && !kart.isFinished())
    			kart.setFinished(true);
    	}

    	// Update  
    	activeUpdate();
    }

	/** Update information of the actives in the game.
	 */
	public void activeUpdate() {
		for (TempObject active : active_effects) {
			active.update(this);

			// If an active's duration has expired, call its
			// dispose() method before completely removing
			// it from the active list.
			if (active.getTimer().isExpired()) {
				active.dispose();
				active_effects.remove(active);
			}
		}
	}

    /** Render the entire screen, so it reflects the current game state.
     * @param g The Slick graphics object, used for drawing.
     */
    public void render(Graphics g, Game game)
    throws SlickException
    {
    	// Let the Camera class draw the world, with its coordinates
    	camera.render(main_player.getPosition());

    	// Render each item into the world
    	for (Item item : items) {
    		item.render(this);
    	}

    	// Render any active effects (projectiles etc.)
		for (TempObject active : active_effects) {
			active.render(this);
		}

    	// Render all players in the game
    	main_player.render(this);
    	for (ComputerPlayer ai : computers) {
    		ai.render(this);
    	}

    	// Render status panel
    	panel.render(g, rank.getRank(main_player), main_player.getItem());

    	// If player has finished the race, draw the finish message
    	rank.render(this, g);
    }

   /** Get the camera for this World.
    * @return A Camera instance related to this World.
    */
   public Camera getCamera() {
	   return camera;
   }
   
   /** Get the game map data related to this World.
    * @return A TiledMap of the map of a World
    */
   public TiledMap getMap() {
	   return map;
   }

   /** Get a list of unpicked items on the map. */
   public ArrayList<Item> getItems() {
	   return items;
   }

   /** Retrieve the human player in this game. */
   public Player getMainPlayer() {
	   return main_player;
   }

   /** Get all computer players in this game. */
   public ArrayList<ComputerPlayer> getComputers() {
	   return computers;
   }

   /** Get the <code>Rank</code> instance responsible for handling
    * ranks of players. */
   public Rank getRank() {
	   return rank;
   }

   /** Get the default list of waypoints for an AI to follow. */
   public ArrayList<Point2D.Double> getWaypoints() {
	   return waypoints;
   }

   /** Get the friction of a tile, from a <code>point</code> in world coordinates
	 * @param tile_point A point in world coordinates
	 * @return The friction coefficient of the point's tile
	 */
   public double frictionAt(Point2D.Double point) {
		// Get the point in tile coordinates, then find the tile's ID
		Point2D.Double tile_point = camera.toTilePoint(point);
		int tile_id = map.getTileId((int)tile_point.x, (int)tile_point.y, 0);

		// ... and then use the tile's ID to get its friction coefficient
		return Double.parseDouble(
				map.getTileProperty(tile_id, PROPERTY_FRICTION, null));
	}

	/** Get the closest kart, based on a point.
	 * @param kartIgnore The kart to ignore in the comparison.
	 * @param point A point to compare with.
	 */
	public Kart getClosestKart(Kart kartIgnore, Point2D.Double point) {
		ArrayList<Kart> all_karts = rank.getKartsSorted();

		// Sort karts based on distance from the point
	   	Collections.sort(all_karts, new Comparator<Kart>() {
	    			public int compare(Kart kart_1, Kart kart_2) {
	    				return (int)(point.distance(kart_1.getPosition()) - point.distance(kart_2.getPosition()));
	    			}
	    	});

	   	// Get the closest which is not the kart specified by kartIgnore
	   	for (Kart kart : all_karts) {
	   		if (kart != kartIgnore)
	   			return kart;
	   	}

	   	return null;
	}

	/** Checks if the kart has collided with another kart.
	 * @param kart The kart of interest.
	 * @param point The future point of the kart.
	 * @return true if the future point will cause the kart to crash with
	 * another kart. Otherwise false.
	 */
	public Kart kartAt(Kart kart, Point2D.Double point) {
		Kart nearest_kart = getClosestKart(kart, point);

		// If the player is within COLLISION_RANGE pixels of another kart
		if (nearest_kart != null &&
				nearest_kart.getPosition().distance(point) <= COLLISION_RANGE) {
			return nearest_kart;
		}
		else
			// If no collision against any player
			return null;
	}

	/** Determines whether the player's future position will hit a 'blocking tile',
	 * or another player.
	 * @param kart The player involved in moving to another point.
	 * @param nextPoint The future point of the player.
	 */
    public boolean blockingAt(Kart kart, Point2D.Double nextPoint)
    {
    	if (kartAt(kart, nextPoint) != null) {
    		return true;
    	}

    	// Also check if futurePoint points to a 'blocking tile'
        return blockingAt(nextPoint);
    }

    /** Determines if the point is a tile that is also a 'blocking tile'.
     * @return true if the tile is a blocking tile, and vice-versa.
     */
    public boolean blockingAt(Point2D.Double point) {
    	return frictionAt(point) >= BLOCKING_TILE_FRICTION;
    }

    /** Checks if a kart is within range of an item.
     * @return Returns an item the kart is near it. Otherwise it returns
     * <code>null</code> if not.
     */
    public Item pickUpItem(Kart kart) {
    	// Check if the player's position is within pick-up range of an item
    	for (Item item : items) {
    		if (kart.getPosition().distance(item.getPosition())
    				<= Item.ITEM_PICKUP_RANGE) {

    			items.remove(item);
    			return item;
    		}
    	}

    	// If the player is not within range of any item
    	return null;
    }

    /** Add an item's active effect to be activated. */
    public void activateEffect(TempObject active) {
    	active_effects.add(active);
    }

    /** Remove an item's active effect to be deactivated. */
    public void deactivateEffect(TempObject active) {
    	active_effects.remove(active);
    }
}
