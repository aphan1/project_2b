/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */ 
package game;

import java.awt.geom.Point2D;

/** Contains functions to convert between world, tile and
 * window coordinates. More importantly, it controls the 
 * viewpoint of a World.
 */
public class Camera {
	/* 
	** COORDINATE DEFINITIONS:
	** 
	** -- World coordinates --
	**		Coordinates in terms of the game map. That is, a
	** World's TMX file map size.
	**
	** -- Tile coordinates --
	**		Coordinates in terms of tiles of the game map.
	**
	** -- Window coordinates --
	**		Coordinates in terms of the points in the game window.
	** Since the windowed game is 800 x 600, then we can only have
	** coordinates of this kind within that range.
	 *
	 */

    /** Number of extra tiles to render, in x and y direction */
    private static final int EXTRA_TILES = 2;

	/** Number of tiles needed to render, based on game screen resolution */
	private int[] num_render;
	/** The tile dimensions of a world's TiledMap */
	private int[] tile;

	/** Starting centre point in world coordinates */
	private Point2D.Double centre;

	/** The world the camera records its viewpoint */
	private World world;

	/** Create a new instance of a World object
	 * @param world The camera for a World
	 * @param centre Initial centre of the camera
	 */
	public Camera(World world, Point2D.Double centre) {
		this.centre = centre;
		this.world = world;

		// Get dimensions of the size of a tile
		tile = new int[] { world.getMap().getTileWidth(),
				world.getMap().getTileHeight() };

		/* Note here that to have the screen fully rendered (while using only a
         * minimum amount of tiles to render), a ceiling function is used to
         * avoid a portion of area on the screen being unrendered.
         * 
         * Moreover, an extra EXTRA_TILES tiles are made to account for prevent
         * rendering a portion of a tile, in case a player decides to
         * press the UP key, for example.
         */
		int x_tiles = (int)Math.ceil((double)StateHandler.getWidth()/tile[0]) + EXTRA_TILES;
		int y_tiles = (int)Math.ceil((double)StateHandler.getHeight()/tile[1]) + EXTRA_TILES;

        num_render = new int[] { x_tiles, y_tiles };
	}

	/** Get the current centre of the camera, in terms of world
	 * coordinates.
	 * <p>
	 * Note: The 'current centre' accounts for the initial centre,
	 * and the offset.
	 * </p>
	 */
	public Point2D.Double getCentre() {
		return centre;
	}

	/** Get the top-left point of the centre of camera. */
	private Point2D.Double getTopLeft() {
		// The amount to subtract to get the top-left point of viewpoint's centre
		return new Point2D.Double(
				centre.x - StateHandler.getWidth()/2.0,
				centre.y - StateHandler.getHeight()/2.0);
	}

	/** Change the initial centre of the camera
	 * @param centre The point to become the new camera's centre
	 */
	public void setCentre(Point2D.Double centre) {
		this.centre = centre;
	}

	/** Converts a world coordinate into a window coordinate, so that
	 * the window coordinate can be rendered in terms of the game's
	 * screen resolution.
	 * @param worldPoint The point to convert into world coordinates
	 * @return <code>worldPoint</code> in terms of windows coordinates
	 */
	public Point2D.Double toWindowPoint(Point2D.Double worldPoint) {
		// This is basically subtracting the top-left point of the camera's
		// viewpoint (in world coordinates), from worldPoint
		return new Point2D.Double(
				worldPoint.x - getTopLeft().x,
				worldPoint.y - getTopLeft().y);
	}

	/** Convert a world coordinate into tile coordinates.
	 * @param point A point in world coordinates
	 * @return Returns the point in terms of tile coordinates
	 */
	public Point2D.Double toTilePoint(Point2D.Double point) {
		return new Point2D.Double(point.getX()/tile[0], point.getY()/tile[1]);
	}

	/** Render the game for a World, based on a centre.
	 * @param centre Centre to render on.
	 */
	public void render(Point2D.Double centre) {
		setCentre(centre);
		render();
	}

	/** Render the game map from a World. */
	public void render() {
		world.getMap().render(
				-(int)getTopLeft().getX() % tile[0],
				-(int)getTopLeft().getY() % tile[1],
				(int)getTopLeft().getX() / tile[0],
				(int)getTopLeft().getY() / tile[1],
				num_render[0],
				num_render[1]);
	}
}
