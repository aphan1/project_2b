/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */
package core;

import java.awt.geom.Point2D;
import java.io.*;
import java.util.ArrayList;

import game.World;
import items.*;

/** Represents an item in Shadow Kart.
 */
public abstract class Item extends GameObject {
	/** Pixel range for a player to pick up an item. */
	public static final double ITEM_PICKUP_RANGE = 40;
	/** Delimiters of a line representing an item. */
	private static final String LINE_DELIMITER = ",";
	/** The initial facing angle of item, in degrees */
	private static final double INITIAL_ANGLE = 0;

	/** User of the item. */
	private Kart user;
	/** The world the item's active effects are taking place */
	private World world;

	/** Creates a new instance of an item.
	 * @param name Name of the item.
	 * @param image_path Full file location of item's image.
	 * @param location Unpicked point location of item (in world coordinates).
	 */
	public Item(String name, String imagePath, Point2D.Double location)
	{
		super(name, imagePath, location, Angle.fromDegrees(INITIAL_ANGLE));
	}

	/** Find out what type of item it is.
	 * @param item_name Name of the time.
	 * @param x X-coordinate of item, in world coordinates.
	 * @param y Y-coordinate of item, in world coordinates.
	 * @return A derived class of Item, based on <code>item_name</code>.
	 * Returns <code>null</code> if it does not match any known items.
	 */
	private static Item deduceItem(String item_name, int x, int y)
	{
		Point2D.Double location = new Point2D.Double(x, y);

		if (item_name.equals(OilCan.ITEM_NAME))
			return new OilCan(location);
		else if (item_name.equals(Boost.ITEM_NAME))
			return new Boost(location);
		else if (item_name.equals(Tomato.ITEM_NAME))
			return new Tomato(location);
		else
			// If the item_name matches no known items
			return null;
	}

	/** Return a list of items, based on a item data file.
	 * <p>
	 * Each line in the file should be in the form: <code>ItemName, x, y</code>.
	 * </p>
	 * <p>
	 * <code>ItemName</code> represents the item's name, whereas <code>x</code>
	 * and <code>y</code> are 2D world coordinates of an item.
	 * </p>
	 * @param file_location Full file location of the data file.
	 * @return A list of items parsed from a file. The order of items in the
	 * list is based on the line number of the item's line in the file.
	 */
	public static ArrayList<Item> getItems(String file_location)
	{
		ArrayList<Item> item_list = new ArrayList<Item>();
		String line;
		
		try {
			// Use a BufferedReader to read the file
			FileReader reader = new FileReader(file_location);
			BufferedReader inFile = new BufferedReader(reader);

			// Read the file line-by-line
			while ((line = inFile.readLine()) != null) {
				// If the line is empty, then skip this line
				if (line.isEmpty())
					continue;

				/* Split the line into an array of the item's properties,
				 * where the line follows this syntax:
				 * [item_name], [x], [y]
				 */
				String[] line_data = line.split(LINE_DELIMITER);
				String item_name = line_data[0];

				// Remove white spacing first so that it can be parsed into a integer
				int x = Integer.parseInt(line_data[1].trim());
				int y = Integer.parseInt(line_data[2].trim());

				Item line_item = deduceItem(item_name, x, y);

				// If the line is a known item
				if (line_item != null) {
					item_list.add(line_item);
				}
			}

			// TODO: Is this the correct way to close off the stream?
			inFile.close();
		}
		catch (FileNotFoundException exception)  {
			System.out.println (exception);
		}
		catch (IOException exception) {
			System.out.println (exception);
		}

		return item_list;
	}

	/** Make an item active, activated by a player.
	 * @param world The world the player is in.
	 * @param user Player who is using this item.
	 */
	public void use(World world, Kart user)
	{
		this.user = user;
		this.world = world;
		activate();
	}

	/** Add an item's active to the item.
	 * @param active An effect of an item (temporary or permanent).
	 */
	protected void addActive(TempObject active)
	{
		world.activateEffect(active);
	}

	/** Get the user of this item. */
	public Kart getUser() {
		return user;
	}

	/** Activate an item (ie. enable its active effects of the item). */
	public abstract void activate();
}
