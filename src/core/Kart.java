/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */
package core;

import java.awt.geom.Point2D;

import game.World;

/** A kart or player, in Shadow Kart.
 */
public abstract class Kart extends GameObject {
	/** Player's default rotational speed, in radians per ms. */
	public static final double DEFAULT_ROTATE_SPEED = 0.004;
	/** The default acceleration of kart, in pixels per ms^2. */
	public static final double DEFAULT_ACCELERATION = 0.0005;

	/** The <code>move_dir</code> value for moving forward. */
	public static final int MOVE_UP = 1;
	/** The <code>rotate_dir</code> value for rotating clockwise. */
	public static final int ROTATE_CLOCKWISE = 1;

	/** The deceleration per second of the kart, should it cross
	 * the finish line. */
	private static final double DECELERATION_FINISH  = 0.0001;
	/** The initial facing angle of player, in degrees. */
	private static final double INITIAL_ANGLE = 0;

	/** If the player has finished the racing game. */
	private boolean finished;
	/** Current accleration of player. */
	private double acceleration;
	/** Current rotational speed of player. */
	private double rotate_speed;
	/** Velocity of player's kart, in pixels per millisecond. */
	private double velocity;

	/** If the kart can use an item */
	private boolean can_use_item;
	/** The item the player currently possesses */
	private Item item;

	/** The move_dir to override the player's move_dir, if
	 * specified. By default, it is assigned as <code>Double.NaN</code>,
	 * meaning that the player has control of its movement. */
	private double move_dir_override;
	/** The rotate_dir to override the player's rotate_dir, if
	 * specified. By default, it is assigned as <code>Double.NaN</code>,
	 * meaning that the player has control of its rotation. */
	private double rotate_dir_override;


	/** Create a new kart for Shadow Kart.
	 * @param name The name of kart.
	 * @param imagePath Full file path of the kart's image.
	 * @param point Initial starting point of kart (world coordinates).
	 * @param canUseItem If the kart can use an item
	 */
	public Kart(String name, String imagePath, Point2D.Double point, boolean canUseItem) {
		super(name, imagePath, point, Angle.fromDegrees(INITIAL_ANGLE));

		this.can_use_item = canUseItem;
		reset();
	}

	/** Have the kart have its properties (velocity, angle etc.)
	 * reset to its initial default values.
	 */
	public void reset() {
		velocity = 0;
		acceleration = DEFAULT_ACCELERATION;
		rotate_speed = DEFAULT_ROTATE_SPEED;

		rotate_dir_override = Double.NaN;
		move_dir_override = Double.NaN;

		this.setAngle(Angle.fromDegrees(INITIAL_ANGLE));
		finished = false;
	}

	/** Get if the kart has finished the race. */
	public boolean isFinished() {
		return finished;
	}

	/** Get the item the player currently has */
	public Item getItem() {
		return item;
	}

	/** Set a new constant accceleration of kart. */
	public void setAcceleration(double value) {
		acceleration = value;
	}

	/** Set a new overriding <code>move_dir</code> value of kart. */
	public void setMoveDirOverride(double move_dir) {
		move_dir_override = move_dir;
	}

	/** Set a new overriding <code>rotate_dir</code> value of kart. */
	public void setRotateDirOverride(double rotate_dir) {
		rotate_dir_override = rotate_dir;
	}

	/** Set a new rotation speed of kart. */
	public void setRotateSpeed(double value) {
		rotate_speed = value;
	}

	/** Set if the kart has finished the race or not. */
	public void setFinished(boolean value) {
		finished = value;
	}

	/** Update the player's position and facing angle.
	 * @param world The world the player is in.
	 * @param rotate_dir Refer to <code>Game.update(...)</code>.
	 * @param move_dir Refer to <code>Game.update(...)</code>.
	 * @param use_item Refer to <code>Game.update(...)</code>.
	 */
	public void move(World world, double rotate_dir, double move_dir, boolean use_item)
	{
		// Override the player's move_dir and rotate_dir input, if
		// move_dir_override and/or rotate_dir_override have been specified
		if (!Double.isNaN(move_dir_override)) {
			move_dir = move_dir_override;
		}
		if (!Double.isNaN(rotate_dir_override)) {
			rotate_dir = rotate_dir_override;
		}

		// If player crosses the finish line, decelerate gradually to a halt
		if (finished) {
			move_dir = 0;
			rotate_dir = 0;

			if (acceleration > 0)
				acceleration -= DECELERATION_FINISH;
			else
				acceleration = 0;
		}

		// If the player can and wants to use an item
		if (can_use_item && use_item && getItem() != null) {
			item.use(world, this);
			item = null;
		}

		// Get friction of tile, based on player's position
		double tile_friction = world.frictionAt(getPosition());

		// Calculate new velocity and angle of player's kart based on player input
		Angle new_angle = getAngle().add(Angle.fromRadians(rotate_speed * rotate_dir));
		setAngle(new_angle);
		velocity = (velocity + acceleration * move_dir) * (1 - tile_friction);

		// Now calculate the change of position, from kart's current position
		Point2D.Double change_vector = new Point2D.Double (
				getAngle().getXComponent(velocity),
				getAngle().getYComponent(velocity));
		Point2D.Double new_position = new Point2D.Double(
				getPosition().getX() + change_vector.getX(),
				getPosition().getY() + change_vector.getY());

		/* If the kart may hit against a 'blocking tile' or 
		 * another player, then prevent the player from crashing.
		 * Otherwise, set the player's point to the new location.
		*/
		if (world.blockingAt(this, new_position)) {
			velocity = 0;
		} else {
			setPosition(new_position);
		}

		// Check if the player can pick-up a nearby item, if it is
		// allowed to use items
		if (can_use_item) {
			Item item_get = world.pickUpItem(this);
			if (item_get != null) {
				item = item_get;
			}
		}
	}
}
