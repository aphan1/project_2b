/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */
package core;

import game.StateHandler;

/** Represents the human player (its kart being the donkey).
 */
public class Player extends Kart {
	/** Default image location of player's kart */
	private static final String DEFAULT_IMAGE = StateHandler.ASSETS_PATH + "/karts/donkey.png";
	/** Default name of player */
	private static final String DEFAULT_NAME = "Donkey";

	/** Create a new human player. */
	public Player() {
		super(DEFAULT_NAME, DEFAULT_IMAGE, null, true);
	}
}
