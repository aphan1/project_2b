/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */
package core;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import game.World;
import computers.*;

/** Represents a computer controlled player.
 */
public abstract class ComputerPlayer extends Kart {
	/** Delimiters of a line representing a waypoint. */
	private static final String LINE_DELIMITER = ",";
	/** Radius range from a way-point to be considered 'close enough'. */
	private static final double WAYPOINT_RANGE = 250;

	/** AI's current 'next destination' point */
	private int current_waypoint;

	/** Create a new generic computer player.
	 * @param name The name of the player.
	 * @param image_path Full image file location of the kart.
	 */
	public ComputerPlayer(String name, String imagePath)
	{
		super(name, imagePath, null, false);
		current_waypoint = 0;
	}

	@Override
	public void reset() {
		current_waypoint = 0;
		super.reset();
	}

	/** Get all working computer players in this project.
	 * @return A list of all available AI players, each which have
	 * been instantiated.
	 */
	public static ArrayList<Kart> getAllComputers() {
		ArrayList<Kart> comp_list = new ArrayList<Kart>();

		comp_list.add(new Elephant());
		comp_list.add(new Dog());
		comp_list.add(new Octopus());

		return comp_list;
	}

	/** Get the current waypoint of the computer player  */
	private Point2D.Double getWaypoint(World world) {
		return world.getWaypoints().get(current_waypoint);
	}

	/** Get a list of points from a file.
	 * <p>
	 * Each line in the file should be in the form: <code>x, y</code>,
	 * where <code>x</code> and <code>y</code> represent integers.
	 * </p>
	 * @param file_location Full file location of the file.
	 * @return A list of points parsed from a file, where it is ordered
	 * based on the line number of a line.
	 */
	public static ArrayList<Point2D.Double> getPoints(String file_location)
	{
		ArrayList<Point2D.Double> waypoints = new ArrayList<Point2D.Double>();
		String line;

		try {
			// Use a BufferedReader to read the file
			FileReader reader = new FileReader(file_location);
			BufferedReader inFile = new BufferedReader(reader);

			// Read the file line-by-line
			while ((line = inFile.readLine()) != null) {
				// If the line is empty, then skip this line
				if (line.isEmpty())
					continue;

				// Split the line into two coordinates, and add that
				// to the list
				String[] point = line.split(LINE_DELIMITER);
				waypoints.add(new Point2D.Double(
						Integer.parseInt(point[0].trim()),
						Integer.parseInt(point[1].trim())));
			}

			// TODO: Is this the correct way to close off the stream?
			inFile.close();
		}
		catch (FileNotFoundException exception)  {
			System.out.println (exception);
		}
		catch (IOException exception) {
			System.out.println (exception);
		}

		return waypoints;
	}

	/** Move the AI kart toward a point <code>dest</code>.
	 * @param world The world the AI kart is in.
	 * @param dest The desired destination point.
	 */
	public void move(World world, Point2D.Double dest)
	{
		/* Calculate the vector between the player and the destination
		 * point, and the vector's angle.
		 */
		Point2D.Double diff = new Point2D.Double(
				dest.getX() - getPosition().getX(),
				dest.getY() - getPosition().getY());
		Angle diff_angle = Angle.fromCartesian(diff.getX(), diff.getY());

		/* First, find the angle between the kart's angle, and the vector 'diff'.
		 * Since the angle returned is between -180 and 180, it is certain that
		 * the lesser angle will be returned.
		 * 
		 * Furthermore, the sign of this lesser angle determines if going left
		 * or right will be faster (in terms of rotating to the angle of 'diff')
		 */
		if (diff_angle.subtract(super.getAngle()).getDegrees() < 0) {
			super.move(world, -Kart.ROTATE_CLOCKWISE, Kart.MOVE_UP, false);
		} else {
			super.move(world, Kart.ROTATE_CLOCKWISE, Kart.MOVE_UP, false);
		}
	}

	/** Move the AI kart toward the current waypoint.
	 * @param world The world the AI kart is in.
	 */
	public void move(World world)
	{
		/* If computer is within WAYPOINT_RANGE radius of a waypoint, forget the
		 * the current waypoint, and move on to the next one. */
		if (current_waypoint < world.getWaypoints().size() - 1 &&
				getPosition().distance(getWaypoint(world)) <= WAYPOINT_RANGE) {
			current_waypoint += 1;
		}

		// Move to the current waypoint
		move(world, getWaypoint(world));
	}
}
