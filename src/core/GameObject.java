package core;

import java.awt.geom.Point2D;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import game.Camera;
import game.World;

/** An object in some world map.
 */
public abstract class GameObject {
	/** The name of the game object. */
	private String name;
	/** The visual image of the object. */
	private Image image;
	/** The Cartesian position of the object, in world coordinates. */
	private Point2D.Double position;
	/** The angular position of the object. */
	private Angle angle;

	public GameObject(String name, String imagePath, Point2D.Double point, Angle angle)
	{
		this.name = name;

		try {
			if (imagePath != null)
				this.image = new Image(imagePath);
		} catch (SlickException e) {}

		this.position = point;
		this.angle = angle;
	}

	/** Get the name of this object. */
	public String getName() {
		return name;
	}

	/** Get the image of the object. */
	public Image getImage() {
		return image;
	}

	/** Get the position of object from some world. */
	public Point2D.Double getPosition() {
		return position;
	}

	/** Get the facing angle of the object. */
	public Angle getAngle() {
		return angle;
	}

	/** Set the new position of the object. */
	public void setPosition(Point2D.Double value) {
		position = value;
	}

	/** Set the facing angle of the object. */
	public void setAngle(Angle value) {
		angle = value;
	}

	@Override
	public String toString() {
		return name;
	}

	/** Render the image representing an object (eg. Kart) into the game.
	 * @param world The world the object is in.
	 */
	public void render(World world)
	{
		if (image == null || position == null) {
			return;
		}

		// Set the new facing angle for the object's image
		image.setRotation((float)angle.getDegrees());

		Camera camera = world.getCamera();
		// Get the position of object in window coordinates
		Point2D.Double window_point = camera.toWindowPoint(position);
  
    	// Now draw image based on kart_position coordinates
    	image.drawCentered((int)window_point.x, (int)window_point.y);
	}
}
