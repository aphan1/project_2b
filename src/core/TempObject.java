/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */
package core;

import java.awt.geom.Point2D;

import game.World;

/** A <code>GameObject</code> that holds a <code>Timer</code>.
 * That is, a temporary object.
 */
public abstract class TempObject extends GameObject {
	/** The default angle of all temporary objects. */
	private static final double DEFAULT_ANGLE = 0;

	/** The timer of a temporary object. */
	private Timer timer;

	/** Create a new temporary game object.
	 * @param imagePath Full file location of the object's image.
	 * @param point The initial location of object (world coordinates).
	 * @param duration How long the object will last for.
	 */
	public TempObject(String imagePath, Point2D.Double point, int duration)
	{
		super(null, imagePath, point, Angle.fromDegrees(DEFAULT_ANGLE));
		timer = new Timer(duration);
	}

	/** Get the timer of the temporary object. */
	public Timer getTimer() {
		return timer;
	}

	/** Update an object's properties (per millisecond).
	 * @param world The world the temporary object is in.
	 */
	public void update(World world) {
		// If object has expired, do nothing.
		// Otherwise, add 1 ms to the object's time elapsed.
		if (timer.isExpired()) return;
		timer.tick();

		// If a subclass of TempObject's update(...) returns
		// true, then expire the object
		if (update(world, timer)) {
			timer.expire();
		}
	}

	@Override
	public void render(World world) {
		if (!timer.isExpired())
			super.render(world);
	}

	/** Update a temporary object's properties.
	 * @param world The world the object is in.
	 * @param timer The timer of the temporary object.
	 * @return If true, then the temporary object will immediately
	 * expire. If false, then the temporary object continues to exist
	 * (as long as time elapsed has not reached its full duration of
	 * the object).
	 */
	public abstract boolean update(World world, Timer timer);

	/** Call final statements before the object becomes expired. */
	public abstract void dispose();
}
