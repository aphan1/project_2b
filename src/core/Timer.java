package core;

/** A timer that must have its time elapsed be incremented.
 */
public class Timer {
	/** Value of <code>duration</code>, such that the timer never expires. */
	public static final int TIME_INFINITE = -1;
	/** Exception text, if <code>duration < 2</code>, from the constructor. */
	private static final String EXCEPTION_TEXT =
			"Duration cannot be a negative value (besides -1).";

	/** Amount of time, such that the timer is considered 'expired' if
	 * <code>time_elapsed > duration</code>. */
	private int duration;
	/** Amount of time that has passed. */
	private int time_elapsed;

	/** Create a new timer object.
	 * @param duration Full length of the timer, after which the timer
	 * becomes 'expired'.
	 */
	public Timer(int duration) {
		this.duration = duration;
		this.time_elapsed = 0;
	}

	/** Expire the timer. */
	public void expire() {
		if (duration == TIME_INFINITE)
			duration = 0;
		else if (duration < TIME_INFINITE - 1)
			throw new IllegalArgumentException(EXCEPTION_TEXT);

		time_elapsed = duration + 1;
	}

	/** Get if the timer has expired. */
	public boolean isExpired() {
		return duration != TIME_INFINITE && time_elapsed > duration;
	}

	/** Manually increment the time elapsed, of the timer. */
	public void tick() {
		time_elapsed++;
	}
}
