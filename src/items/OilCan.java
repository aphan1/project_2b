/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */
package items;

import java.awt.geom.Point2D;
import java.util.ArrayList;

import core.Item;
import core.Kart;
import core.Player;
import core.TempObject;
import core.Timer;
import game.StateHandler;
import game.World;

/** The Tomato item in Shadow Kart.
 */
public class OilCan extends Item {
	/** Name of the item. */
	public static final String ITEM_NAME = "Oil can";
	/** Full file path of the oil can image. */
	private static final String CAN_FILE = StateHandler.ASSETS_PATH  + "/items/oilcan.png";
	/** Full file path of the oil slick image. */
	private static final String SLICK_FILE = StateHandler.ASSETS_PATH + "/items/oilslick.png";

	/** The number of pixels (away from the user) where the oil slick will
	 * be placed. */
	private static final double SLICK_PLACEMENT = 40;
	/** How long the kart will spin for, if it touches the slick. */
	private static final int SPIN_DURATION = 700;
	/** The constant in which the kart spins clockwise. */
	private static final double SLICK_ROTATE_RATE = 0.008;

	/** Create a new oil can item.
	 * @param location Unpicked location of item (world coordinates).
	 */
	public OilCan(Point2D.Double location)
	{
		super(ITEM_NAME, CAN_FILE, location);
	}

	@Override
	public void activate() {
		super.addActive(new OilSlick());
	}

	/** Represents an oil slick (item active). */
	private class OilSlick extends TempObject {
		/** The kart that has touched the oil slick. */
		private Kart affected_kart;

		/** Create a new oil slick object (from an oil can). */
		public OilSlick()
		{
			super(SLICK_FILE, null, Timer.TIME_INFINITE);

			// Set the position of the slick, that is away from the user
			// (determined by SLICK_PLACEMENT and angle of user).
			Point2D.Double point_sub = new Point2D.Double(
					getUser().getAngle().getXComponent(SLICK_PLACEMENT),
					getUser().getAngle().getYComponent(SLICK_PLACEMENT));

			Point2D.Double slick_point = new Point2D.Double(
					getUser().getPosition().x - point_sub.x,
					getUser().getPosition().y - point_sub.y);

			this.setPosition(slick_point);
		}

		@Override
		public boolean update(World world, Timer timer)
		{
			ArrayList<Kart> all_karts = world.getRank().getKartsSorted();

			// Check if any kart is within the pick-up range of the oil slick
			for (Kart k : all_karts) {
				// If the kart has touched the oil slick
	    		if (k.getPosition().distance(getPosition()) <= Item.ITEM_PICKUP_RANGE) {
	    			affected_kart = k;
	    			return true;
	    		}
			}
			return false;
		}

		@Override
		public void dispose()
		{
			// Add another active to make the affected kart spin
			addActive(new OilSlickActive(affected_kart));
		}
	}

	/** The spinning active effect of the oil slick.
	 */
	class OilSlickActive extends TempObject {
		/** The kart who is spinning uncontrollably. */
		private Kart kart;

		/** Creates a new spinning item active effect. */
		public OilSlickActive(Kart affectedKart)
		{
			super(null, null, SPIN_DURATION);
			kart = affectedKart;

			// Set the kart to spin uncontrollably, clockwise.
			kart.setRotateDirOverride(Kart.MOVE_UP);
			kart.setMoveDirOverride(Kart.ROTATE_CLOCKWISE);
			kart.setRotateSpeed(SLICK_ROTATE_RATE);
		}

		@Override
		public void dispose() {
			// Give control to the kart, when the spinning effect
			// ends
			kart.setMoveDirOverride(Double.NaN);
			kart.setRotateDirOverride(Double.NaN);
			kart.setRotateSpeed(Player.DEFAULT_ROTATE_SPEED);	
		}

		@Override
		public boolean update(World world, Timer timer) {
			return false;
		}
	}
}
