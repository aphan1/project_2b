/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */
package items;

import java.awt.geom.Point2D;

import core.Item;
import core.Kart;
import core.Player;
import core.TempObject;
import core.Timer;
import game.StateHandler;
import game.World;

/** The Boost item in Shadow Kart.
 */
public class Boost extends Item {
	/** Name of the item. */
	public static final String ITEM_NAME = "Boost";
	/** Filename of the item's image. */
	private static final String BOOST_FILE = StateHandler.ASSETS_PATH + "/items/boost.png";
	/** How long this item lasts for, when used (in milliseconds) */
	private static final int DURATION = 4000;
	/** New additional acceleration of player, when item is active */
	private static final double BOOST_ACCELERATION = 0.0008;

	/** Create a new Boost item on a map.
	 * @param location Location of item (in world coordinates).
	 */
	public Boost(Point2D.Double location)
	{
		super(ITEM_NAME, BOOST_FILE, location);
	}

	@Override
	public void activate()
	{
		super.addActive(new BoostActive());
	}

	/** An active of the Boost item.
	 */
	private class BoostActive extends TempObject {
		/** Create a new instance of the Boost's active. */
		public BoostActive()
		{
			super(null, null, DURATION);
			getUser().setAcceleration(BOOST_ACCELERATION);
			getUser().setMoveDirOverride(Kart.MOVE_UP);
		}

		@Override
		public void dispose() {
			getUser().setAcceleration(Player.DEFAULT_ACCELERATION);
			getUser().setMoveDirOverride(Double.NaN);				
		}

		@Override
		public boolean update(World world, Timer timer) {
			return false;
		}
	}
}
