/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */
package items;

import java.awt.geom.Point2D;

import core.Item;
import core.Kart;
import core.TempObject;
import core.Timer;
import game.StateHandler;
import game.World;

/** The Tomato item in Shadow Kart.
 */
public class Tomato extends Item {
	/** Name of the item. */
	public static final String ITEM_NAME = "Tomato";
	/** Full file path of the item's image. */
	private static final String TOMATO_FILE = StateHandler.ASSETS_PATH + "/items/tomato.png";
	/** Full file path of the item's projectile image. */
	private static final String PROJECTILE_FILE = StateHandler.ASSETS_PATH + "/items/tomato-projectile.png";
	/** The number of pixels (away from the user) where the tomato's projectile
	 * will be placed.
	 * Note that the value is 50, not 40, to avoid the user from being affected by the item.
	 */
	private static final int PROJECTILE_PLACE = 50;
	/** Constant velocity of the tomato projectile. */
	private static final double VELOCITY = 1.7;

	/** Create a new Tomato item. 
	 * @param location Unpicked item location of Tomato (world coordinates).
	 */
	public Tomato(Point2D.Double location)
	{
		super(ITEM_NAME, TOMATO_FILE, location);
	}

	@Override
	public void activate()
	{
		addActive(new TomatoProjectile());
	}

	/** The tomato projectile of Tomato (item active).
	 */
	private class TomatoProjectile extends TempObject {
		/** The kart that has been hit by the projectile. */
		private Kart affected_kart;

		/** Create a new tomato projectile. */
		public TomatoProjectile()
		{
			super(PROJECTILE_FILE, null, Timer.TIME_INFINITE);

			// Set the angle of the tomato projectile to the user's
			// projectile.
			setAngle(getUser().getAngle());

			// Set the initial position of the projectile ahead of
			// the player
			Point2D.Double point_add = new Point2D.Double(
					getAngle().getXComponent(PROJECTILE_PLACE),
					getAngle().getYComponent(PROJECTILE_PLACE));

			Point2D.Double initial_point = new Point2D.Double(
					getUser().getPosition().x + point_add.x,
					getUser().getPosition().y + point_add.y);

			setPosition(initial_point);
		}

		@Override
		public boolean update(World world, Timer timer) {
			// If projectile has hit a 'blocking tile'
			if (world.blockingAt(getPosition())) {
				return true;
			}

			// If the projectile has hit a kart
			Kart closest_kart = world.kartAt(null, getPosition());
			if (closest_kart != null) {
				affected_kart = closest_kart;
				return true;
			}
			
			Point2D.Double point_add = new Point2D.Double(
					getAngle().getXComponent(VELOCITY),
					getAngle().getYComponent(VELOCITY));

			Point2D.Double new_position = new Point2D.Double(
					getPosition().x + point_add.x,
					getPosition().y + point_add.y);

			setPosition(new_position);
			return false;
		}

		@Override
		public void dispose() {
			if (affected_kart == null) return;

			// If the projectile has hit a kart, reuse the Oil item's
			// active effect to make the affected kart spin uncontrollably
			OilCan oilcan = new OilCan(null);
			addActive(oilcan.new OilSlickActive(affected_kart));
		}
	}
}
