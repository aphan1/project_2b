/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Matt Giuca <mgiuca>
 * Edited by: Andy Phan <aphan1>
 */
package states;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.EmptyTransition;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import core.Kart;
import de.matthiasmann.twl.Button;
import de.matthiasmann.twl.DialogLayout;
import de.matthiasmann.twl.ProgressBar;
import game.StateHandler;
import game.World;
import twl.BasicTWLGameState;
import twl.RootPane;

import org.newdawn.slick.Input;

/** Main class for the Shadow Kart Game engine.
 * Handles initialisation, input, GUI rendering and Slick rendering.
 */
public class Game extends BasicTWLGameState
{
	/** Game state ID for this class. */
	private static final int STATE_ID = 0;

	/** Text for the button <code>btnReplay</code>. */
	private static final String BTNREPLAY_TEXT = "Restart game";
	/** Text for the button <code>btnReturn</code>. */
	private static final String BTNRETURN_TEXT = "Restart to Main Menu";

    /** Transparency of the black rectangle rendered over the background,
     * when the game is paused. */
    private static final float BLACK_RECTANGLE_OPACITY = 0.6f;
    /** Text to indicate game is paused. */
    private static final String PAUSED_TEXT = "GAME PAUSED";
    /** Text to tell how to unpause game. */
    private static final String PAUSED_INFO_TEXT = "Press ESC to unpause";
    /** Ratio of the screen height of where to place the pause text. */
	private static final float PAUSED_TEXT_RATIO = 5;
    /** Ratio of the screen height of where to place the pause info text. */
	private static final float PAUSED_INFO_TEXT_RATIO = 1.8f;

    /** The game state. */
    private World world;

    /** Panel containing all buttons (when the game is paused). */
    private DialogLayout dlPanel;
    /** Button to restart game. */
    private Button btnReplay;
    /** Button to return back to main menu. */
    private Button btnReturn;
    /** Progressbar for the player's health. */
    private ProgressBar health_bar;

    /** The karts in the game. */
    private Kart[] player_data;
    /** Whether the game is paused or not. */
	private boolean paused;

    @Override
    protected RootPane createRootPane() {
        RootPane rp = super.createRootPane();

        // Panel containing all buttons for pause menu
        dlPanel = new DialogLayout();
        dlPanel.setTheme("");

        // Restart game button
        btnReplay = new Button(BTNREPLAY_TEXT);
        // When the button is pressed, restart the game with a transition,
        // followed by telling World to reset its properties.
        btnReplay.addCallback(new Runnable() {
            public void run() {
                try {
                	StateHandler.getInstance().enterState(
                			getID(), new EmptyTransition(), new FadeInTransition());

                	reset();
					
				} catch (SlickException e) {
					e.printStackTrace();
				}
            }
        });

        // Return to main menu button
        btnReturn = new Button(BTNRETURN_TEXT);
        btnReturn.addCallback(new Runnable() {
            public void run() {
                StateHandler.getInstance().enterState(
                		StateHandler.getMainMenu().getID(),
                		new FadeOutTransition(),
                		new EmptyTransition());
            }
        });

        // TODO: Should I add this
        health_bar = new ProgressBar();
        health_bar.setValue(100);

        // Add all buttons to the panel
        dlPanel.setHorizontalGroup(dlPanel.createParallelGroup()
        		.addWidget(btnReplay)
        		.addWidget(btnReturn));
        dlPanel.setVerticalGroup(dlPanel.createSequentialGroup()
        		.addWidget(btnReplay)
        		.addWidget(btnReturn));

        // Make the pause buttons initially invisible
        dlPanel.setVisible(false);

        // Add all widgets to rp (widgets to render)
        rp.add(dlPanel);
        rp.add(health_bar);

        return rp;
    }

    @Override
    public void enter(GameContainer container, StateBasedGame game) {
    	// When entering this game, reset the game
    	try {
    		super.enter(container, game);
    		reset();
		} catch (SlickException e) {
			e.printStackTrace();
		}
    }

    /** Set the karts to be in this game. */
    public void setPlayerData(Kart[] data) {
    	player_data = data;
    }

    @Override
    protected void layoutRootPane() {
    	health_bar.adjustSize();
    	// Place pause panel to the near centre of screen
    	dlPanel.adjustSize();
    	dlPanel.setPosition(
    			StateHandler.getWidth()/2 - dlPanel.getWidth()/2, 
    			StateHandler.getHeight()/2 - dlPanel.getHeight());
    }

    /** Initialise the game state.
     * @param gc The Slick game container object.
     */
    @Override
    public void init(GameContainer gc, StateBasedGame state)
    throws SlickException
    {
        world = new World();
    }

    /** Update the game state for a frame.
     * @param gc The Slick game container object.
     * @param delta Time passed since last frame (milliseconds).
     */
    @Override
    public void update(GameContainer gc, StateBasedGame state, int delta)
    throws SlickException
    {
        // Get data about the current input (keyboard state).
        Input input = gc.getInput();

        // Update the player's rotation and position based on key presses.
        double rotate_dir = 0;
        double move_dir = 0;
        if (input.isKeyDown(Input.KEY_DOWN) || input.isKeyDown(Input.KEY_S))
            move_dir -= 1;
        if (input.isKeyDown(Input.KEY_UP) || input.isKeyDown(Input.KEY_W))
            move_dir += 1;
        if (input.isKeyDown(Input.KEY_LEFT) || input.isKeyDown(Input.KEY_A))
            rotate_dir -= 1;
        if (input.isKeyDown(Input.KEY_RIGHT) || input.isKeyDown(Input.KEY_D))
            rotate_dir += 1;
        boolean use_item = input.isKeyDown(Input.KEY_LCONTROL) ||
            input.isKeyDown(Input.KEY_RCONTROL);
        boolean toggle_pause = input.isKeyPressed(Input.KEY_ESCAPE);

        // Toggle pause
    	if (toggle_pause) {
    		this.paused = !this.paused;
    	}

        // Let World.update decide what to do with this data.
        for (int i=0; i<delta; i++)
            world.update(rotate_dir, move_dir, use_item, paused);

        // If main player has finished the race, then user cannot
        // pause the game
    	if (world.getMainPlayer().isFinished()) {
    		paused = false;
    		dlPanel.setVisible(true);
    	}
    }

    /** Render the entire screen, so it reflects the current game state.
     * @param gc The Slick game container object.
     * @param g The Slick graphics object, used for drawing.
     */
    @Override
    public void render(GameContainer gc, StateBasedGame state, Graphics g)
    throws SlickException
    {
        // Let World.render handle the rendering.
        world.render(g, this);

        // Render the pause menu, if main player has not finished the race
        if (!world.getMainPlayer().isFinished())
        	togglePause(g);
    }

	@Override
	public int getID() {
		return STATE_ID;
	}

    private void togglePause(Graphics g) {
    	// Make pause panel visible (if game is paused)
    	dlPanel.setVisible(paused);
        health_bar.setVisible(!paused);
    	
    	if (paused) {
    		// Render a somewhat black rectangle over the game (if paused)
    		Color color = new Color(0f, 0f, 0f, BLACK_RECTANGLE_OPACITY);
    		g.setColor(color);
            g.fillRect(0,0, StateHandler.getWidth(), StateHandler.getHeight());

            // Render text, indicating game is paused
            g.setColor(Color.yellow);
            g.drawString(PAUSED_TEXT,
        			StateHandler.getWidth()/2 - g.getFont().getWidth(PAUSED_TEXT)/2,
        			StateHandler.getHeight()/PAUSED_TEXT_RATIO);
            
            g.drawString(PAUSED_INFO_TEXT,
        			StateHandler.getWidth()/2 - g.getFont().getWidth(PAUSED_INFO_TEXT)/2,
        			StateHandler.getHeight()/PAUSED_INFO_TEXT_RATIO);
    	} else {
    		g.setColor(Color.white);
    	}
    }

    /** Reset the game. 
     * @throws SlickException */
    private void reset()
    throws SlickException {
    	paused = false;
    	dlPanel.setVisible(false);
		world.start(player_data);
    }
}
