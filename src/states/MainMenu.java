/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */
package states;

import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.EmptyTransition;
import org.newdawn.slick.state.transition.FadeInTransition;

import de.matthiasmann.twl.Button;
import de.matthiasmann.twl.DialogLayout;
import game.StateHandler;
import twl.BasicTWLGameState;
import twl.RootPane;

/** State representing the main menu.
 */
public class MainMenu extends BasicTWLGameState {
	/** Full file image location of the main menu's background. */
	private static final String BACKGROUND_FILE = StateHandler.ASSETS_PATH + "/menu_bg.jpg";
	/** Game state ID for this class. */
	private static final int STATE_ID = 1;

	/** Panel containing all buttons in this game state. */
    private DialogLayout dlPanel;
	/** Button to start a default single player game. */
    private Button btnPlay;
    /** Button to prepare a custom single player game. */
    private Button btnCustom;
    /** Button to look at credits. */
    private Button btnCredits;
    /** Button to exit the game. */
    private Button btnExit;

    /** Text for button <code>btnPlay</code>. */
    private static final String BTNPLAY_TEXT = "Play single player (default)";
    /** Text for button <code>btnCustom</code>. */
    private static final String BTNCUSTOM_TEXT = "Play custom game";
    /** Text for button <code>btnCredits</code>. */
    private static final String BTNCREDITS_TEXT = "Credits";
    /** Text for button <code>btnExit</code>. */
    private static final String BTNEXIT_TEXT = "Exit to desktop";

    /** Font size of text of title. */
    private static final int FONT_TITLE_SIZE = 50;
	/** Offset of where the font title is rendered. */
	private static final int[] FONT_TITLE_OFFSET = { 10, 0 };
    /** Transparency of the black rectangle rendered over the background. */
    private static final float BLACK_RECTANGLE_OPACITY = 0.6f;

    /** Background for this game state. */
    private Image background;
    /** The font for the title of this game state. */
    private UnicodeFont font_title;

    @Override
    protected RootPane createRootPane() {
        RootPane rp = super.createRootPane();

        // Prepare panel
        dlPanel = new DialogLayout();
        dlPanel.setTheme("");

        // Play button
        btnPlay = new Button(BTNPLAY_TEXT);
        btnPlay.addCallback(new Runnable() {
            public void run() {
            	StateHandler.getInstance().enterState(
            			StateHandler.getGame().getID(),
            			new EmptyTransition(),
            			new FadeInTransition());
            }
        });

        // Play custom game button
        btnCustom = new Button(BTNCUSTOM_TEXT);
        btnCustom.addCallback(new Runnable() {
            public void run() {
                StateHandler.getInstance()
                			.enterState(StateHandler.getSelectKarts().getID());
            }
        });

        // Credits button
        btnCredits = new Button(BTNCREDITS_TEXT);
        btnCredits.addCallback(new Runnable() {
            public void run() {
                StateHandler.getInstance()
                			.enterState(StateHandler.getCredits().getID());
            }
        });

        // Exit to desktop button
        btnExit = new Button(BTNEXIT_TEXT);
        btnExit.addCallback(new Runnable() {
            public void run() {
                System.exit(0);
            }
        });

        // Arrange the buttons vertically
        dlPanel.setHorizontalGroup(dlPanel.createParallelGroup()
        		.addWidget(btnPlay)
        		.addWidget(btnCustom)
        		.addWidget(btnCredits)
        		.addWidget(btnExit));
        dlPanel.setVerticalGroup(dlPanel.createSequentialGroup()
        		.addWidget(btnPlay)
        		.addWidget(btnCustom)
        		.addWidget(btnCredits)
        		.addWidget(btnExit));

        // Add panel to rp (the widgets to render)
        rp.add(dlPanel);

        return rp;
    }

    @Override
    protected void layoutRootPane() {
    	// Adjust size of panel, and place it to centre of screen
    	dlPanel.adjustSize();
    	dlPanel.setPosition(
    			StateHandler.getWidth()/2 - dlPanel.getWidth()/2,
    			StateHandler.getHeight()/2 - dlPanel.getHeight()/2);
    	
    }

	@SuppressWarnings("unchecked")
	@Override
	public void init(GameContainer gc, StateBasedGame state)
	throws SlickException {
		// Load up the background for the main menu
		background = new Image(BACKGROUND_FILE);

		// Initialise the title font
		Font font = new Font(StateHandler.FONT_NAME, Font.ITALIC, FONT_TITLE_SIZE);
		font_title = new UnicodeFont(font);
		font_title.getEffects().add(new ColorEffect(java.awt.Color.white));
		font_title.addAsciiGlyphs();
		font_title.loadGlyphs();
	}

	@Override
	public void render(GameContainer gc, StateBasedGame state, Graphics g)
	throws SlickException {
		// Render background
		background.draw();

		// Render a black rectangle above the background
		Color color = new Color(0f, 0f, 0f, BLACK_RECTANGLE_OPACITY);
		g.setColor(color);
        g.fillRect(0,0, StateHandler.getWidth(), StateHandler.getHeight());

        // And finally the title of this game state
        font_title.drawString(
        		StateHandler.getWidth() - font_title.getWidth(StateHandler.GAME_NAME) - FONT_TITLE_OFFSET[0],
        		FONT_TITLE_OFFSET[1], StateHandler.GAME_NAME,
        		Color.white);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame state, int delta)
	throws SlickException {
		// Nothing to update, on the Slick side of things.
	}

	@Override
	public int getID() {
		return STATE_ID;
	}
}
