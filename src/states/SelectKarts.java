/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */
package states;

import java.awt.Font;
import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.EmptyTransition;
import org.newdawn.slick.state.transition.FadeInTransition;

import core.ComputerPlayer;
import core.Kart;
import core.Player;
import de.matthiasmann.twl.Button;
import de.matthiasmann.twl.ComboBox;
import de.matthiasmann.twl.DialogLayout;
import de.matthiasmann.twl.DialogLayout.Group;
import de.matthiasmann.twl.Label;
import de.matthiasmann.twl.model.SimpleChangableListModel;
import game.StateHandler;
import twl.BasicTWLGameState;
import twl.RootPane;

/** A game state, representing a custom game selection menu.
 */
public class SelectKarts extends BasicTWLGameState {
	/** Full file image location of the main menu's background. */
	private static final String BACKGROUND_FILE = StateHandler.ASSETS_PATH + "/kartselect_bg.png";
	/** Title for this game state. */
	private static final String TITLE_NAME = "Custom game mode";
	/** Game state ID for this class. */
	private static final int STATE_ID = 2;

	/** Text for the button <code>btnBack</code>. */
	private static final String BTNBACK_TEXT = "Back";
	/** Text for the button <code>btnPlay</code>. */
	private static final String BTNPPLAY_TEXT = "Play!";
	/** Text for the label <code>lblNumPlayers</code>. */
	private static final String LBLNUMPLAYERS_TEXT = "Players:";
	/** Text for the label <code>lblPlayers</code>. */
	private static final String LBLPLAYERS_TEXT = "Player ";
	/** Theme name for a label with white coloured text. */
	private static final String THEME_LBLWHITE = "label_white";

	/** Position of <code>dlPanel</code>. */
	private static final int[] DLPANEL_POSITION = { 50, 70 };
	/** Constant offset of <code>btnBack</code>. */
	private static final int[] BTNBACK_OFFSET = { 10, 10 };
	/** Constant offset of <code>dlNumPlayers</code>. */
	private static final int[] DLNUMPLAYERS_OFFSET =
		{ 10, 15 };
	/** Minimum width for each <code>cbPlayers</code>. */
	private static final int MIN_WIDTH_CBPLAYERS = 100;

	/** Horizontal gap between <code>lblPlayers</code> and <code>cbPlayers</code>. */
	private static final int HORIZONTAL_GAP = 100;
	/** Vertical gap for each row in <code>dlPanel</code>. */
	private static final int VERTICAL_GAP = 25;

	/** Font size for the title. */
	private static final int FONT_TITLE_SIZE = 20;
	/** Position where the font title is rendered (by Slick). */
	private static final int[] FONT_TITLE_POSITION = { 10, 10 };
    /** Transparency of the black rectangle rendered over the background. */
    private static final float BLACK_RECTANGLE_OPACITY = 0.7f;

	/** Panel containing <code>lblPlayers</code> and <code>cbPlayers</code>. */
	private DialogLayout dlPanel;
	/** List of labels, representing a player in the menu. */
	private ArrayList<Label> lblPlayers;
	/** List of comboboxes, representing a player's kart. */
	private ArrayList<ComboBox<Kart>> cbPlayers;

	/** Panel containing <code>lblNumPlayers</code> and <code>cbNumPlayers</code>. */
	private DialogLayout dlNumPlayers;
	/** Label indicating the context for <code>cbNumPlayers</code>. */
	private Label lblNumPlayers;
	/** Combobox allowing user to change number of players in the custom game. */
	private ComboBox<Integer> cbNumPlayers;

	/** Button to go back to main menu. */
	private Button btnBack;
	/** Button to start the game. */
	private Button btnPlay;

	/** Background for this game state. */
	private Image background;

	/** Font for the this game state's title. */
	private UnicodeFont font_title;

    @Override
    protected RootPane createRootPane() {
        RootPane rp = super.createRootPane();

        // Back button to Main Menu
        btnBack = new Button(BTNBACK_TEXT);
        btnBack.addCallback(new Runnable() {
            public void run() {
                StateHandler.getInstance()
                			.enterState(StateHandler.getMainMenu().getID());
            }
        });

        // Panel (and its child controls) for customising player karts
        dlPanel = new DialogLayout();
        dlPanel.setTheme("");

        Group main_horizontal_group = dlPanel.createParallelGroup();
        Group main_vertical_group = dlPanel.createSequentialGroup();

        // Initialise a list of labels and comboboxes for the players
        lblPlayers = new ArrayList<Label>(StateHandler.MAX_PLAYERS);
        cbPlayers = new ArrayList<ComboBox<Kart>>(StateHandler.MAX_PLAYERS);

        // For each player, load up a label and combobox, and add that to the
        // panel's rows.
        for (int i = 1; i <= StateHandler.MAX_PLAYERS; i++) {
            Group horizontal_group = dlPanel.createSequentialGroup();
            Group vertical_group = dlPanel.createParallelGroup();

            ComboBox<Kart> cbPlayer = createComboBox(i);
            cbPlayers.add(cbPlayer);

            Label lblName = new Label(LBLPLAYERS_TEXT + i);
            lblName.setTheme(THEME_LBLWHITE);
            lblPlayers.add(lblName);

            horizontal_group
            			.addWidget(lblName)
            			.addGap(HORIZONTAL_GAP)
            			.addWidget(cbPlayer);

            vertical_group
            			.addWidget(lblName)
            			.addWidget(cbPlayer);

            main_horizontal_group.addGroup(horizontal_group);
            main_vertical_group
            			.addGroup(vertical_group)
            			.addGap(VERTICAL_GAP);
        }

        dlPanel.setHorizontalGroup(main_horizontal_group);
        dlPanel.setVerticalGroup(main_vertical_group);

        // Label for number of players in a game
        lblNumPlayers = new Label(LBLNUMPLAYERS_TEXT);
        lblNumPlayers.setLabelFor(cbNumPlayers);
        lblNumPlayers.setTheme(THEME_LBLWHITE);

        // Combobox for the number of players
        cbNumPlayers = new ComboBox<Integer>();
        SimpleChangableListModel<Integer> cbNumPlayers_list =
        		new SimpleChangableListModel<Integer>();

        for (int j = 1; j <= StateHandler.MAX_PLAYERS; j++)
        	cbNumPlayers_list.addElement(j);
        cbNumPlayers.setModel(cbNumPlayers_list);

        // Add an event listener, if the user changes the value in the combobox
        cbNumPlayers.addCallback(new Runnable() {
            public void run() {
            	int num_players = cbNumPlayers.getSelected();

                for (int k = 0; k < StateHandler.MAX_PLAYERS; k++) {
                	boolean visible = (k <= num_players);

                	lblPlayers.get(k).setVisible(visible);
                	cbPlayers.get(k).setVisible(visible);
                }
            }
        });

        // Panel containing lblNumPlayers and cbNumPlayers
        dlNumPlayers = new DialogLayout();
        dlNumPlayers.setTheme("");
        dlNumPlayers.setHorizontalGroup(dlNumPlayers.createSequentialGroup()
        		.addWidget(lblNumPlayers)
        		.addGap(10)
        		.addWidget(cbNumPlayers));
        dlNumPlayers.setVerticalGroup(dlNumPlayers.createParallelGroup()
        		.addWidget(lblNumPlayers)
        		.addWidget(cbNumPlayers));


        // Button to start the game
        btnPlay = new Button(BTNPPLAY_TEXT);
        btnPlay.addCallback(new Runnable() {
            public void run() {
            	prepareGame();
            }
        });

        // Add the widgets to rp, which handles the widgets to render
        rp.add(btnBack);
        rp.add(dlPanel);
        rp.add(dlNumPlayers);
        rp.add(btnPlay);

        // Set the number of players to the default value
        cbNumPlayers.setSelected(StateHandler.DEFAULT_PLAYERS - 1);
        return rp;
    }

    /** Prepare the game, based on custom game input. */
    private void prepareGame() {
    	ArrayList<Kart> karts = new ArrayList<Kart>();

    	// Add all players selected for the custom game, and their karts
    	for (ComboBox<Kart> box : cbPlayers) {
    		if (!box.isVisible())
    			break;

    		karts.add(box.getModel().getEntry(box.getSelected()));
    		// Send the players chosen to the game state Game.
    		StateHandler.getGame()
    					.setPlayerData(karts.toArray(new Kart[karts.size()]));
    	}

    	// Enter into the game
    	StateHandler.getInstance()
    				.enterState(StateHandler.getGame().getID(),
    							new EmptyTransition(),
    							new FadeInTransition());
    }


    /** Creates and returns a combobox for a player.
     * @param playerNumber The player's number.
     */
    private ComboBox<Kart> createComboBox(int playerNumber) {
    	ComboBox<Kart> cb_player = new ComboBox<Kart>();
 
    	SimpleChangableListModel<Kart> cb_list = new SimpleChangableListModel<Kart>();
    	cb_player.setModel(cb_list);

    	// If playerNumber indicates a human player, then only put Donkey into the list
    	if (playerNumber == 1) {
    		cb_list.addElement(new Player());
    		cb_player.setSelected(0);
    	}  else {
    		// If it is a computer player
    		ArrayList<Kart> computers = ComputerPlayer.getAllComputers();

        	cb_list.addElements(computers);
        	cb_player.setSelected((playerNumber - 1) % computers.size());
    	}

    	return cb_player;
    }

    @Override
    protected void layoutRootPane() {
    	// Adjust size and set positions of all widgets
    	dlPanel.setPosition(DLPANEL_POSITION[0], DLPANEL_POSITION[1]);
    	dlPanel.adjustSize();

    	for (ComboBox<Kart> box : cbPlayers) {
    		box.setMinSize(MIN_WIDTH_CBPLAYERS, box.getHeight());
    	}

    	btnBack.adjustSize();
    	btnBack.setPosition(
    			StateHandler.getWidth() - btnBack.getWidth() - BTNBACK_OFFSET[0],
    			BTNBACK_OFFSET[1]);
    	
    	btnPlay.adjustSize();
    	btnPlay.setPosition(
    			btnBack.getX() - btnPlay.getWidth() - BTNBACK_OFFSET[0],
    			BTNBACK_OFFSET[1]);

    	dlNumPlayers.adjustSize();
    	dlNumPlayers.setPosition(
    			StateHandler.getWidth() - dlNumPlayers.getWidth() - DLNUMPLAYERS_OFFSET[0],
    			dlNumPlayers.getHeight() + DLNUMPLAYERS_OFFSET[1]);
    }

	@SuppressWarnings("unchecked")
	@Override
	public void init(GameContainer gc, StateBasedGame state) throws SlickException {
		// Load up the background for this game state
		background = new Image(BACKGROUND_FILE);

		// Initialise the font for rendering
		Font font = new Font(StateHandler.FONT_NAME, Font.PLAIN, FONT_TITLE_SIZE);
		font_title = new UnicodeFont(font);
		font_title.getEffects().add(new ColorEffect(java.awt.Color.white));
		font_title.addAsciiGlyphs();
		font_title.loadGlyphs();
	}

	@Override
	public void render(GameContainer gc, StateBasedGame state, Graphics g)
	throws SlickException {
		background.draw();
		Color color = new Color(0f, 0f, 0f, BLACK_RECTANGLE_OPACITY);
		g.setColor(color);
        g.fillRect(0, 0, StateHandler.getWidth(), StateHandler.getHeight());

        font_title.drawString(FONT_TITLE_POSITION[0], FONT_TITLE_POSITION[1], TITLE_NAME);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame state, int delta)
	throws SlickException {
        // Get data about the current input (keyboard state).
        Input input = gc.getInput();
        int current = cbNumPlayers.getSelected();

        // If player has pressed the '+' keyboard button, then increase
        // number of players in custom game
        if (input.isKeyPressed(Input.KEY_ADD) &&
        		current < cbNumPlayers.getModel().getNumEntries() - 1) {
        	cbNumPlayers.setSelected(++current);
        }

        // ... and vice-versa for the keyboard button '-'.
        if (input.isKeyPressed(Input.KEY_SUBTRACT) &&
        		current > 0) {
        	cbNumPlayers.setSelected(--current);
        }
	}

	@Override
	public int getID() {
		return STATE_ID;
	}
}
