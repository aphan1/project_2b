/* SWEN20003 Object Oriented Software Development
 * Shadow Kart
 * Author: Andy Phan <aphan1>
 */
package states;

import java.awt.Font;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.state.StateBasedGame;

import de.matthiasmann.twl.Button;
import game.StateHandler;
import twl.BasicTWLGameState;
import twl.RootPane;

/** Game state representing the credits of the game.
 */
public class Credits extends BasicTWLGameState {
	/** Full file image location of the main menu's background. */
	private static final String BACKGROUND_FILE = StateHandler.ASSETS_PATH + "/menu_bg.jpg";
	/** Filename for credits text. */
	private static final String CREDITS_FILENAME = "credits.txt";
	/** Game state ID for this class. */
	private static final int STATE_ID = 3;

	/** Text for the button <code>btnBack</code>. */
	private static final String BTNBACK_TEXT = "Back";

	/** Constant offset of <code>btnBack</code>. */
	private static final int[] BTNBACK_OFFSET = { 10, 10 };
	/** Starting y-coordinate of the text of credits */
	private static final int CREDITS_Y_START = 50;
	/** The amount of space between two lines of text. */
	private static final int CREDITS_SPACING = 30;
    /** Transparency of the black rectangle rendered over the background. */
    private static final float BLACK_RECTANGLE_OPACITY = 0.8f;
    /** Font size of text of title. */
    private static final int FONT_TITLE_SIZE = 18;

    /** The credits text to be rendered, separated by line. */
    private List<String> credits_text;
    /** Background for this game state. */
    private Image background;

	/** Button to go back to main menu. */
	private Button btnBack;
    /** The font for credit text. */
    private UnicodeFont font_credits;

    @Override
    protected RootPane createRootPane() {
        RootPane rp = super.createRootPane();

        // Back button to Main Menu
        btnBack = new Button(BTNBACK_TEXT);
        btnBack.addCallback(new Runnable() {
            public void run() {
                StateHandler.getInstance()
                			.enterState(StateHandler.getMainMenu().getID());
            }
        });

        // Load up the credits text from the file
        Path path = Paths.get(StateHandler.WORKING_DIR + "\\" + StateHandler.DATA_PATH,
        		CREDITS_FILENAME);
        try {
			credits_text = Files.readAllLines(path);
		} catch (IOException e) {
			e.printStackTrace();
		}

        rp.add(btnBack);

        return rp;
    }

    @Override
    protected void layoutRootPane() {
    	// Adjust size, and position the back button
    	btnBack.adjustSize();
    	btnBack.setPosition(
    			StateHandler.getWidth() - btnBack.getWidth() - BTNBACK_OFFSET[0],
    			BTNBACK_OFFSET[1]);
    }

	@SuppressWarnings("unchecked")
	@Override
	public void init(GameContainer gc, StateBasedGame state)
	throws SlickException {
		// Load up the background for the main menu
		background = new Image(BACKGROUND_FILE);

		// Initialise the credits font
		Font font = new Font(StateHandler.FONT_NAME, Font.PLAIN, FONT_TITLE_SIZE);
		font_credits = new UnicodeFont(font);
		font_credits.getEffects().add(new ColorEffect(java.awt.Color.white));
		font_credits.addAsciiGlyphs();
		font_credits.loadGlyphs();
	}

	@Override
	public void render(GameContainer gc, StateBasedGame state, Graphics g)
	throws SlickException {
		// Render background
		background.draw();

		// Render a black rectangle above the background
		Color color = new Color(0f, 0f, 0f, BLACK_RECTANGLE_OPACITY);
		g.setColor(color);
        g.fillRect(0,0, StateHandler.getWidth(), StateHandler.getHeight());

        // Now draw text, line by line (centred)
        int text_rendered = 0;
        for (String text : credits_text) {
        	font_credits.drawString(
        			StateHandler.getWidth()/2 - font_credits.getWidth(text)/2,
        			CREDITS_Y_START + CREDITS_SPACING * text_rendered,
        			text);

        	text_rendered++;
        }
	}

	@Override
	public void update(GameContainer gc, StateBasedGame state, int delta)
	throws SlickException {
		// Nothing to update, on the slick side of things.
	}

	@Override
	public int getID() {
		return STATE_ID;
	}
}
